﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyPlant : MonoBehaviour
{

    [SerializeField] private Spore sporePrefab;
    [SerializeField] private int sporeCount;
    [SerializeField] private List<Spore> sporeStore;
    [SerializeField] private Transform sporeSpawn;
    [SerializeField] private Animator animator;
    [SerializeField] private bool readyToFire;
    [SerializeField] private float forceX;
    [SerializeField] private float forceY;
    [SerializeField] private int randMin;
    [SerializeField] private int randMax;
    [SerializeField] private int directionMin;
    [SerializeField] private int directionMax;



    private void Start()
    {
        sporeStore = new List<Spore>();
        for(int i = 0; i < sporeCount; i++)
        {
            var sporeTemp = Instantiate(sporePrefab, sporeSpawn);
            sporeStore.Add(sporeTemp);
            sporeTemp.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        FireReady();
    }

    public void FireReady()
    {
        if (readyToFire)
        {
            animator.SetTrigger("IsAttack");
            readyToFire = false;
        }
    }

    public void Fire()
    {
        StartCoroutine(ReadyToFire());
        StartCoroutine(SporeFire());
    }
    private Spore GetSporeFromStore()
    {
        if (sporeStore.Count > 0)
        {
            var spore = sporeStore[0];
            sporeStore.Remove(spore);
            spore.transform.parent = null;
            spore.transform.position = sporeSpawn.position;
            spore.gameObject.SetActive(true);

            if(spore.ReturnSpore == null)
                spore.ReturnSpore += ReturnSporetoStore;

            return spore;
        }
        else
            return Instantiate(sporePrefab, sporeSpawn.position, Quaternion.identity);
    }

    private void ReturnSporetoStore(Spore spore)
    {
        sporeStore.Add(spore);
        spore.transform.parent = sporeSpawn.transform;
        spore.transform.localPosition = Vector3.zero;
        spore.gameObject.SetActive(false);
    }


    IEnumerator ReadyToFire()
    {
        yield return new WaitForSeconds(5);
        readyToFire = true;
        yield break;
    }
    IEnumerator SporeFire()
    {
        int count = sporeStore.Count;
        int shift = sporeCount/2;
        int typeOfAttack = Random.Range(randMin, randMax);
        if (typeOfAttack > 20)
        {
            int direction = 0;
            while (direction == 0)
            {
                direction = Random.Range(directionMin, directionMax);
            }

            for (int i = 0; i < sporeCount; i++)
            {
                var currentSpore = GetSporeFromStore();

                if (direction > 0)
                    currentSpore.SetImpulse(1, shift, forceY);
                else if (direction < 0)
                    currentSpore.SetImpulse(-1, shift * (-1), forceY);

                yield return new WaitForSeconds(0.5f);
            }
            yield break;
        }
        else if (typeOfAttack > 10)
        {
            for (int i = 0; i < sporeCount; i++)
            {
                var currentSpore = GetSporeFromStore();

                if(shift < 0)
                    currentSpore.SetImpulse(-1, shift, forceY);
                else if(shift == 0)
                    currentSpore.SetImpulse(0, shift, forceY);
                else if(shift > 0)
                    currentSpore.SetImpulse(1, shift, forceY);
                shift -= 1;
            }
            yield break;
        }
        else
        {
            int direction = 0;
            while(direction == 0)
            {
                direction = Random.Range(-10, 10);
            }
            Debug.Log("Направление - (" + direction + ");");
            for (int i = 0; i < sporeCount; i++)
            {
                var currentSpore = GetSporeFromStore();
                if(direction > 0)
                    currentSpore.SetImpulse(1, shift * (-1), forceY);
                else if(direction < 0)
                    currentSpore.SetImpulse(-1, shift, forceY);
                shift--;
                yield return new WaitForSeconds(0.5f);
            }
            yield break;
        }

    }
}

