﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressedButton : MonoBehaviour
{
    private bool isPressed;
    public Action ButtonUp;

    public bool IsPressed 
    {
        get { return isPressed; }
        set { isPressed = value; }
    }

    public void OnPointerDown()
    {
        isPressed = true;
    }

    public void OnPointerUp()
    {
        isPressed = false;

        if (ButtonUp != null)
            ButtonUp();
    }
}
