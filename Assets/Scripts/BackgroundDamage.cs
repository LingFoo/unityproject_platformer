﻿using System.Collections;
using UnityEngine;

public class BackgroundDamage : MonoBehaviour
{
    private bool damageCooldown;
    [SerializeField] private byte damage;

    private void OnTriggerStay2D(Collider2D col)
    {

            if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
            {
                var health = GameManager.Instance.healthContainer[col.gameObject];
                if (!damageCooldown)
                {
                    damageCooldown = true;
                    StartCoroutine(AreaDamageCooldown());
                    health.TakeHit(damage);
                }
            }
        
    }

    IEnumerator AreaDamageCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        damageCooldown = !damageCooldown;
    }

}
