﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aid : MonoBehaviour
{

    [SerializeField] private int aidPower;
    public int AidPower
    {
        get { return aidPower; }
        set { aidPower = value; }
    }
    [SerializeField] private Animator animator;
    [SerializeField] private BoxCollider2D boxCollider;


    private void Start()
    {
        GameManager.Instance.aidContainer.Add(gameObject, this);
    }
    public void StartAnimator()
    {
        animator.SetTrigger("Destroy");
        boxCollider.enabled = false;
    }
    public void StartDestroy()
    {
        Destroy(gameObject);
    }
}
