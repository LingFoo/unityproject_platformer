﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UICharacterController : MonoBehaviour
{
    [SerializeField] private PressedButton left;
    [SerializeField] private PressedButton right;
    [SerializeField] private PressedButton pressedJump;
    [SerializeField] private Button jump;
    [SerializeField] private Button fire;

    public PressedButton Left { get { return left; } }
    public PressedButton Right { get { return right; } }
    public PressedButton PressedJump { get { return pressedJump; } }
    public Button Jump { get { return jump; } }
    public Button Fire { get { return fire; } }

    private void Start()
    {
        Player.Instance.InitUIController(this);
    }
}
