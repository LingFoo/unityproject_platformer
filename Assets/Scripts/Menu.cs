﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private InputField nameField;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject levelMenu;
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject returnButton;

    [SerializeField] private List<Button> levelButtons;

    private GameObject selectedMenu;

    public static Menu Instance;

    private void Awake()
    {
        Instance = this;

        //levelButtons = new List<GameObject>();
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("Player_Name"))
            nameField.text = PlayerPrefs.GetString("Player_Name");

        int num = PlayerPrefs.GetInt("Levels", 1);

        foreach(Button levelButton in levelButtons)
        {
            levelButton.interactable = false;
        }

        for(int i = 0; i < num; i++)
        {
            levelButtons[i].interactable = true;
        }
    }
    public void OnEndEditName()
    {
        PlayerPrefs.SetString("Player_Name", nameField.text);
    }
    public void OnClickPlay()
    {
        SceneManager.LoadScene(1);
    }

    public void OnClickExit()
    {
        Application.Quit();
    }
    public void InClickLevels() 
    {
        selectedMenu = levelMenu;
        mainMenu.SetActive(false);
        levelMenu.SetActive(true);
        returnButton.SetActive(true);
    }

    public void OnClickOptions()
    {
        selectedMenu = optionsMenu;
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
        returnButton.SetActive(true);
    }
    public void OnClickReturn()
    {
        mainMenu.SetActive(true);
        selectedMenu.SetActive(false);
        returnButton.SetActive(false);

        selectedMenu = null;
    }

    public void OnClickRestartTutorial()
    {
        PlayerPrefs.SetInt("right", 0);
        PlayerPrefs.SetInt("left", 0);
        PlayerPrefs.SetInt("jump", 0);
        PlayerPrefs.SetInt("ladder", 0);
        PlayerPrefs.SetInt("wolf", 0);
        PlayerPrefs.SetInt("fire", 0);
        PlayerPrefs.SetInt("glass", 0);
        PlayerPrefs.SetInt("force", 0);
        PlayerPrefs.SetInt("armor", 0);
        PlayerPrefs.SetInt("damage", 0);
        PlayerPrefs.SetInt("moss", 0);
        PlayerPrefs.SetInt("spore", 0);
        OnClickReturn();
    }

    public void OnClickFirstLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void OnClickSecondLevel()
    {
        SceneManager.LoadScene(2);
    }

    public void OnClickThirdLevel()
    {
        SceneManager.LoadScene(3);
    }

    public void OnClickFourthLevel()
    {
        SceneManager.LoadScene(4);
    }

    public void OnClickFivthLevel()
    {
        SceneManager.LoadScene(5);
    }

    public void OnClickSixthLevel()
    {
        SceneManager.LoadScene(6);
    }

    public void OnClickSeventhLevel()
    {
        SceneManager.LoadScene(7);
    }
}
