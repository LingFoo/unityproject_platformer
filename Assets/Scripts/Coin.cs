﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private BoxCollider2D boxCollider;

    public void Start()
    {
        GameManager.Instance.coinContainer.Add(gameObject, this);
    }
    public void StartDestroy()
    {
        boxCollider.enabled = false;
        animator.SetTrigger("Destroy");
    }

    public void EndDestroy()
    {
        Destroy(gameObject);
    }

    public void OfflineCollider()
    {
        boxCollider.enabled = false;
    }
}
