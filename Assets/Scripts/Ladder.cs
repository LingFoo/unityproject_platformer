﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.GetComponent<Player>())
        {
            Player.Instance.OnLadder = true;
        }
    }


    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.GetComponent<Player>())
        {
            Player.Instance.OnLadder = false;
        }
    }
}
