﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spore : MonoBehaviour
{
    [SerializeField] private bool isGrounded;
    private Vector3 vector;
    [SerializeField] private new Rigidbody2D rigidbody;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Animator animator;
    [SerializeField] private bool isDrop;
    [SerializeField] private float speed;
    [SerializeField] private float oldspeed;
    [SerializeField] private float gravity;
    [SerializeField] private Color color;
    public Action<Spore> ReturnSpore;

    private void Start()
    {
        gravity = rigidbody.gravityScale;
        color = spriteRenderer.color;

        if(Instructor.Instanse != null)
            Instructor.Instanse.InitSpore(this);
    }
    private void Update()
    {
        if (!isGrounded)
        {
            if (rigidbody.velocity != Vector2.zero)
            {
                if (rigidbody.velocity.x > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, ((Quaternion.LookRotation(rigidbody.velocity, Vector3.up).eulerAngles.x) - 60) * -1);
                    spriteRenderer.flipX = true;
                }
                else
                {
                    spriteRenderer.flipX = false;
                    transform.rotation = Quaternion.Euler(0, 0, (Quaternion.LookRotation(rigidbody.velocity, Vector3.up).eulerAngles.x) - 60);
                }
            }
        }
        else
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.gravityScale = 0;
        }

        if(rigidbody.velocity.y < speed / 2)
        {
            isDrop = true;
            animator.SetBool("IsDrop", isDrop);
        }
        if(transform.position.y < -200)
        {
            DestroySpore();
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.CompareTag("Enemy"))
        {
            return;
        }
        if ((rigidbody.velocity).y > 0 && col.gameObject.CompareTag("Platform"))
        {
            Debug.Log("Выход из метода");
            return;
        }
        if (col.gameObject.CompareTag("Platform"))
        {
            isGrounded = true;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            animator.SetBool("IsGrounded", isGrounded);

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

    public void SetImpulse(float direction, float forceX, float forceY)
    {
        rigidbody.velocity = Vector3.zero;
        StartCoroutine(SporeTimeLife());
        if (direction != 0)
        {
            rigidbody.AddForce(Vector2.up * forceY, ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.right * forceX, ForceMode2D.Impulse);
        }
        else
        {
            rigidbody.AddForce(Vector2.up * forceY, ForceMode2D.Impulse);
        }
    }

    private void DestroySpore()
    {
        if (ReturnSpore != null)
        {
            rigidbody.gravityScale = gravity;
            spriteRenderer.color = color;
            isDrop = false;
            ReturnSpore(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator SporeTimeLife()
    {
        yield return new WaitForSeconds(4.5f);
        if (gameObject.activeSelf)
        {
            DestroySpore();
        }
    }


}
