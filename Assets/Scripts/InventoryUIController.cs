﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUIController : MonoBehaviour
{
    [SerializeField] private Cell[] cells;
    [SerializeField] private int cellCount;
    [SerializeField] private Cell cellPrefab;
    [SerializeField] private Transform rootParent;
    private void Awake()
    {
        GameManager.Instance.inventoryUI = this;

        cells = new Cell[cellCount];
        for (int i = 0; i < cellCount; i++)
        {
            cells[i] = Instantiate(cellPrefab, rootParent);
            cells[i].gameObject.SetActive(true);
        }
        //cellPrefab.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        InitCells();
    }

    public void InitCells()
    {
        var inventory = GameManager.Instance.inventory;
        //Debug.Log(cells.Length);
        for (int i = 0; i < cells.Length; i++)
        {


            if (i < inventory.Items.Count)
            {
                cells[i].Init(inventory.Items[i]);
                //Debug.Log("SET item" + inventory.Items[i].Icon.name);
            }
            else
                cells[i].Init(null);

        }
    }
}
