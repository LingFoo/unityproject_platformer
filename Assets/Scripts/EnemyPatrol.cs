﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{

    public GameObject leftBorder;
    public GameObject rightBorder;
    public Rigidbody2D rigidbody;
    public GroundDetecter groundDetecter;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public Vector2 vector;
    [SerializeField] private CollisionDamage collisionDamage;

    public bool isRightDirection;

    [SerializeField] private float speed;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.enemyContainer.Add(gameObject, this);
    }

    // Update is called once per frame
    void Update()
    {
        if (groundDetecter.isGrounded)
        {
            if (transform.position.x > rightBorder.transform.position.x || collisionDamage.Direction < 0)
                isRightDirection = false;
            else if (transform.position.x < leftBorder.transform.position.x || collisionDamage.Direction > 0)
                isRightDirection = true;
            rigidbody.velocity = isRightDirection ? Vector2.right : Vector2.left;
            rigidbody.velocity *= speed;
            animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));
        }

        if (rigidbody.velocity.x > 0)
            spriteRenderer.flipX = true;
        if (rigidbody.velocity.x < 0)
            spriteRenderer.flipX = false;
    }
}
