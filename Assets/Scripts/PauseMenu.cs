﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    [SerializeField] private Animator animator;
    private Resolution screenHight;
    private float currentHight;
    private float baseHight;
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button soundButton;
    [SerializeField] private Text soundButtonText;
    private int onOffSound;

    [SerializeField] private GameObject inventoryWindow;
    [SerializeField] private GameObject pauseWindow;
    [SerializeField] private GameObject deathWindow;
    [SerializeField] private GameObject victoryWindow;
    [SerializeField] private Button nextLevelButton;
    public static PauseMenu Instance;

    public Button NextLevelButton
    {
        get { return nextLevelButton; }
        set { nextLevelButton = value; }
    }
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("Player_Sound"))
        {
            onOffSound = PlayerPrefs.GetInt("Player_Sound");
            if (onOffSound == 1)
            {
                soundButtonText.text = "Sound On";
            }
            else
            {
                soundButtonText.text = "Sound Off";
            }
        }
        screenHight = Screen.currentResolution;

        baseHight = rectTransform.anchoredPosition.y;

    }
    public void OpenPauseMenu()
    {

        pauseButton.interactable = false;
        StartCoroutine(OpenMenu());
        Time.timeScale = 0;

    }

    public void ClosePauseMenu()
    {
        pauseButton.interactable = true;
        StartCoroutine(CloseMenu());
    }
    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OnOffSound()
    {
        if (onOffSound == 1)
        {
            onOffSound = 0;
            soundButtonText.text = "Sound Off";
            PlayerPrefs.SetInt("Player_Sound", onOffSound);
        }

        else
        {
            onOffSound = 1;
            soundButtonText.text = "Sound On";
            PlayerPrefs.SetInt("Player_Sound", onOffSound);
        }
    }

    public void OpenInventory()
    {
        if (inventoryWindow.activeSelf)
        {
            inventoryWindow.SetActive(false);
            if (!pauseWindow.activeSelf)
            {
                Time.timeScale = 1;
            }
        }
        else
        {
            inventoryWindow.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void IsDeath()
    {
        deathWindow.SetActive(true);
    }

    public void Victory()
    {
        victoryWindow.SetActive(true);
        Time.timeScale = 0;
    }

    public void NextLevelClick()
    {
        Debug.Log(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void RestartClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Debug.Log(SceneManager.GetActiveScene().name);
    }

    IEnumerator OpenMenu()
    {
        pauseWindow.SetActive(true);
        while (rectTransform.anchoredPosition.y > (screenHight.height / 2) * -1)
        {
            currentHight = rectTransform.transform.localPosition.y;
            yield return new WaitForSecondsRealtime(0.00005f);
            rectTransform.transform.localPosition = new Vector3(0, currentHight - 20, 0);
        }
        Debug.Log(rectTransform.transform.localPosition);
        yield break;
    }

    IEnumerator CloseMenu()
    {

        while (rectTransform.anchoredPosition.y <= baseHight)
        {
            currentHight = rectTransform.transform.localPosition.y;
            yield return new WaitForSecondsRealtime(0.00005f);
            rectTransform.transform.localPosition = new Vector3(0, currentHight + 20, 0);
        }
        pauseWindow.SetActive(false);
        if (!inventoryWindow.activeSelf)
        {
            Time.timeScale = 1;
        }
        yield break;
    }
}
