﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager Instance { get; set; }
    #endregion

    [SerializeField] private GameObject inventoryPanel;
    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject, Coin> coinContainer;
    public Dictionary<GameObject, Aid> aidContainer;
    public Dictionary<GameObject, BuffReciever> buffRecieverContainer;
    public Dictionary<GameObject, EnemyPatrol> enemyContainer;
    public Dictionary<GameObject, ItemComponent> itemsContainer;
    [HideInInspector] public PlayerInventory inventory;
    [HideInInspector] public InventoryUIController inventoryUI;
    public ItemBase itemDataBase;
    [SerializeField] private float endGame;

    [SerializeField] private Spore sporePrefab;

    private void Awake()
    {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();
        coinContainer = new Dictionary<GameObject, Coin>();
        aidContainer = new Dictionary<GameObject, Aid>();
        buffRecieverContainer = new Dictionary<GameObject, BuffReciever>();
        enemyContainer = new Dictionary<GameObject, EnemyPatrol>();
        itemsContainer = new Dictionary<GameObject, ItemComponent>();
    }
    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings - 1)
        {
            PauseMenu.Instance.NextLevelButton.interactable = true;
            Debug.Log("Сцена есть.");
        }
        else
        {
            PauseMenu.Instance.NextLevelButton.interactable = false;
            Debug.Log("Сцены нет.");
        }


    }

    private void Update()
    {
        if(Player.Instance.transform.position.x > endGame && Player.Instance.GroundDetecter.isGrounded)
        {
            PauseMenu.Instance.Victory();
            PlayerPrefs.SetInt("Levels", SceneManager.GetActiveScene().buildIndex + 1);
        }

    }
}
