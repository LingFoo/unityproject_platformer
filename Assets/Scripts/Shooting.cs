﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
    [SerializeField] private bool isShooting;
    public bool IsShooting
    {
        get { return isShooting; }
        set { isShooting = value; }
    }
    [SerializeField] private Arrow arrow;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private new Rigidbody2D rigidbody;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private GameObject player;
    [SerializeField] private Animator animator;
    [SerializeField] private float coolDown;
    [SerializeField] private short arrowsCount = 3;
    [SerializeField] private Image timer;
    private List<Arrow> arrowPool;
    private Arrow currentArrow;


    public static Shooting Instance { get; set; }
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        arrowPool = new List<Arrow>();
        for(int i = 0; i < arrowsCount; i++)
        {
            var arrowTemp = Instantiate(arrow, arrowSpawnPoint);
            arrowPool.Add(arrowTemp);
            arrowTemp.gameObject.SetActive(false);
        }
    }

    private Arrow GetArrowFromPool()
    {
        if(arrowPool.Count > 0)
        {
            var arrowTemp = arrowPool[0];
            arrowPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        return Instantiate(arrow, arrowSpawnPoint.transform.position, Quaternion.identity);
    }

    public void ReturnArrowtoPool(Arrow arrowTemp)
    {
        if (!arrowPool.Contains(arrowTemp))
            arrowPool.Add(arrowTemp);

        arrowTemp.transform.parent = arrowSpawnPoint;
        arrowTemp.transform.position = arrowSpawnPoint.transform.position;
        arrowTemp.gameObject.SetActive(false);
    }

    public void CheckShoot(Vector2 playerVelocity)
    {
        if (playerVelocity == Vector2.zero && isShooting == false)
        {
            isShooting = true;
            PrepareToShoot();
            StartCoroutine(ShootCooldown());
        }
    }

    public void PrepareToShoot()
    {
        animator.SetBool("IsShooting", isShooting);
    }
    public void StartShoot()
    {
        currentArrow = GetArrowFromPool();
        currentArrow.SetImpulse(Vector2.right, (!spriteRenderer.flipX) ? currentArrow.Force : currentArrow.Force * -1, this);
        animator.SetBool("IsShooting", false);


    }

    public void BuffArrows(int buffDamage)
    {
        StartCoroutine(BuffArrowDamage(buffDamage));
    }

    IEnumerator ShootCooldown()
    {
        timer.fillAmount = 1;
        float time = coolDown / 100;
        while (timer.fillAmount > 0)
        {
            timer.fillAmount -= 0.01f;
            yield return new WaitForSeconds(time);
        }
        isShooting = !isShooting;
        yield break;
    }


    IEnumerator BuffArrowDamage(int buffDamage)
    {
        for (int i = 0; i < arrowsCount; i++)
        {
            arrowPool[i].TriggerDamage.Damage += buffDamage;
        }
        Debug.Log("Увеличенный урон стрелы - " + arrowPool[0].TriggerDamage.Damage);
        yield return new WaitForSeconds(10);

        for (int i = 0; i < arrowsCount; i++)
        {
            arrowPool[i].TriggerDamage.Damage -= buffDamage;
        }
        Debug.Log("Стандартный урон стрелы - " + arrowPool[0].TriggerDamage.Damage);
        yield break;
    }
}
