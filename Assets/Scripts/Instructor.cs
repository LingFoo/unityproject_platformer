﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Instructor : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject cursor;
    [SerializeField] private Text cursorText;
    [SerializeField] private RectTransform left;
    [SerializeField] private RectTransform right;
    [SerializeField] private RectTransform jump;
    [SerializeField] private RectTransform fire;
    [SerializeField] private GameObject wolf;
    [SerializeField] private GameObject ladder;
    [SerializeField] private GameObject glass;
    [SerializeField] private GameObject potion;
    [SerializeField] private BuffType potionType;
    [SerializeField] private GameObject mossAmanita;
    [SerializeField] private List<GameObject> spore;
    [SerializeField] private GameObject enemyPlant;

    [SerializeField] private bool leftComplite;
    [SerializeField] private bool rightComplite;
    [SerializeField] private bool jumpComplite;
    [SerializeField] private bool fireComplite;
    [SerializeField] private bool wolfComplite;
    [SerializeField] private bool ladderComplite;
    [SerializeField] private bool glassComplite;
    [SerializeField] private bool armorPotionComplete;
    [SerializeField] private bool forcePotionComplete;
    [SerializeField] private bool damagePotionComplete;
    [SerializeField] private bool mossAmanitaComplete;
    [SerializeField] private bool sporeComplite;

    private string leftText = "Для передвижения влево используйте кнопку двжиения влево(в правой части экрана).";
    private string rightText = "Для передвижения вправо используйте кнопку двжиения вправо(в правой части экрана).";
    private string jumpText = "Для прыжка используйте кнопку прыжка(в левой части экрана).";
    private string fireText = "Используйте кнопку стрельбыпрыжка(в левой части экрана) чтобы атаковать волка.";
    private string wolfText = "Волк. Атакует при конткате.";
    private string ladderText = "Для подъёма по лестнице используйте кнопку прыжка.";
    private string glassText = "Осколки. Наносят урон при касании и отбрасывают в сторону.";
    private string forcePotionText = "Зелье силы. Используйте его чтобы увеличить силу прыжка";
    private string damagePotionText = "Зелье урона. Используйте его чтобы увеличить урон от стрел";
    private string armorPotionText = "Зелье защиты. Используйте его чтобы увеличить защиту от урона";
    private string mossAmanitaText = "Ядовитый мох. Наносит периодический урон при касании.";
    private string sporeText = "Берегитесь спор хищных растений!";

    private bool timerTutorial = true;

    public static Instructor Instanse;

    public void InitSpore(Spore spore)
    {
        this.spore.Add(spore.gameObject);
    }
    private void Awake()
    {
        Instanse = this;
        spore = new List<GameObject>();
    }

    private void Start()
    {
        if(PlayerPrefs.GetInt("right", 0) == 1)
        {
            rightComplite = true;
        }

        if(PlayerPrefs.GetInt("left", 0) == 1)
        {
            leftComplite = true;
        }
        
        if (PlayerPrefs.GetInt("jump", 0) == 1)
        {
            jumpComplite = true;
        }

        if (PlayerPrefs.GetInt("ladder", 0) == 1)
        {
            ladderComplite = true;
        }

        if (PlayerPrefs.GetInt("wolf", 0) == 1)
        {
            wolfComplite = true;
        }

        if (PlayerPrefs.GetInt("fire", 0) == 1)
        {
            fireComplite = true;
        }

        if (PlayerPrefs.GetInt("glass", 0) == 1)
        {
            glassComplite = true;
        }

        if(PlayerPrefs.GetInt("force", 0) == 1)
        {
            forcePotionComplete = true;
        }

        if (PlayerPrefs.GetInt("armor", 0) == 1)
        {
            armorPotionComplete = true;
        }

        if (PlayerPrefs.GetInt("damage", 0) == 1)
        {
            damagePotionComplete = true;
        }

        if (PlayerPrefs.GetInt("moss", 0) == 1)
        {
            mossAmanitaComplete = true;
        }

        if (PlayerPrefs.GetInt("spore", 0) == 1)
        {
            sporeComplite = true;
        }
    }

    private void Update()
    {
        if (!rightComplite)
        {
            TutorialCourse(right, rightComplite, rightText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                rightComplite = !rightComplite;
                PlayerPrefs.SetInt("right", 1);
            }
#endif
            if(Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touch.phase = TouchPhase.Ended;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    rightComplite = !rightComplite;
                    PlayerPrefs.SetInt("right", 1);
                }
            }
        }

        if (!leftComplite && rightComplite && timerTutorial)
        {
            TutorialCourse(left, leftComplite, leftText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                leftComplite = !leftComplite;
                PlayerPrefs.SetInt("left", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touch.phase = TouchPhase.Ended;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    leftComplite = !leftComplite;
                    PlayerPrefs.SetInt("left", 1);
                }
            }
        }


        if (player != null && player.transform.position.x > 6 && !jumpComplite)
        {
            TutorialCourse(jump, jumpComplite, jumpText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                jumpComplite = !jumpComplite;
                PlayerPrefs.SetInt("jump", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touch.phase = TouchPhase.Ended;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    jumpComplite = !jumpComplite;
                    PlayerPrefs.SetInt("jump", 1);
                }
            }
        }

        if (ladder != null && Camera.main.WorldToViewportPoint(ladder.transform.position).x < 0.9 && !ladderComplite)
        {
            TutorialCourse(ladder, ladderComplite, ladderText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                ladderComplite = !ladderComplite;
                PlayerPrefs.SetInt("ladder", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touch.phase = TouchPhase.Ended;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    ladderComplite = !ladderComplite;
                    PlayerPrefs.SetInt("ladder", 1);
                }
            }
        }

        if (wolf != null && Camera.main.WorldToViewportPoint(wolf.transform.position).x < 0.9 && !wolfComplite)
        {
            TutorialCourse(wolf, wolfComplite, wolfText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                wolfComplite = !wolfComplite;
                PlayerPrefs.SetInt("wolf", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touch.phase = TouchPhase.Ended;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    wolfComplite = !wolfComplite;
                    PlayerPrefs.SetInt("wolf", 1);
                }
            }
        }

        if (!fireComplite && wolfComplite && timerTutorial)
        {
            TutorialCourse(fire, fireComplite, fireText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                fireComplite = !fireComplite;
                PlayerPrefs.SetInt("fire", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touch.phase = TouchPhase.Ended;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    fireComplite = !fireComplite;
                    PlayerPrefs.SetInt("fire", 1);
                }
            }
        }

        if (glass != null && Camera.main.WorldToViewportPoint(glass.transform.position).x < 0.9 && Camera.main.WorldToViewportPoint(glass.transform.position).y > 0.2 && !glassComplite)
        {
            Debug.Log("Осколки");
            TutorialCourse(glass, glassComplite, glassText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                glassComplite = !glassComplite;
                PlayerPrefs.SetInt("glass", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    glassComplite = !glassComplite;
                    PlayerPrefs.SetInt("glass", 1);
                }
            }
        }

        if (potion != null && potionType == BuffType.Force)
        {
            if (!forcePotionComplete && Camera.main.WorldToViewportPoint(potion.transform.position).x < 0.6)
            {
                TutorialCourse(potion, forcePotionComplete, forcePotionText);
#if UNITY_EDITOR
                if (Input.GetMouseButtonDown(0))
                {
                    NextCourse();
                    forcePotionComplete = !forcePotionComplete;
                    PlayerPrefs.SetInt("force", 1);
                }
#endif
                if (Input.touchCount > 0)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        NextCourse();
                        forcePotionComplete = !forcePotionComplete;
                        PlayerPrefs.SetInt("force", 1);
                    }
                }
            }
        }
        else if (potion != null && potionType == BuffType.Damage)
        {
            if (!damagePotionComplete && Camera.main.WorldToViewportPoint(potion.transform.position).x < 0.6)
            {
                TutorialCourse(potion, damagePotionComplete, damagePotionText);
#if UNITY_EDITOR
                if (Input.GetMouseButtonDown(0))
                {
                    NextCourse();
                    damagePotionComplete = !damagePotionComplete;
                    PlayerPrefs.SetInt("damage", 1);
                }
#endif
                if (Input.touchCount > 0)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        NextCourse();
                        damagePotionComplete = !damagePotionComplete;
                        PlayerPrefs.SetInt("damage", 1);
                    }
                }
            }
        }
        else if (potion != null && potionType == BuffType.Armor)
        {
            if (!armorPotionComplete && Camera.main.WorldToViewportPoint(potion.transform.position).x < 0.4 && Camera.main.WorldToViewportPoint(potion.transform.position).y < 0.4)
            {
                TutorialCourse(potion, armorPotionComplete, armorPotionText);
#if UNITY_EDITOR
                if (Input.GetMouseButtonDown(0))
                {
                    NextCourse();
                    armorPotionComplete = !armorPotionComplete;
                    PlayerPrefs.SetInt("armor", 1);
                }
#endif
                if (Input.touchCount > 0)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        NextCourse();
                        armorPotionComplete = !armorPotionComplete;
                        PlayerPrefs.SetInt("armor", 1);
                    }
                }
            }
        }

        if(mossAmanita != null && !mossAmanitaComplete && Camera.main.WorldToViewportPoint(mossAmanita.transform.position).x < 0.9)
        {
            TutorialCourse(mossAmanita, mossAmanitaComplete, mossAmanitaText);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                NextCourse();
                mossAmanitaComplete = !mossAmanitaComplete;
                PlayerPrefs.SetInt("moss", 1);
            }
#endif
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    NextCourse();
                    mossAmanitaComplete = !mossAmanitaComplete;
                    PlayerPrefs.SetInt("moss", 1);
                }
            }
        }

        if(spore.Count > 0 && !sporeComplite)
        {
            for(int i = 0; i < spore.Count; i++)
            {
                if (spore[i] != null && spore[i].activeSelf && Camera.main.WorldToViewportPoint(spore[i].transform.position).x < 0.7)
                {
                    if (i + 1 <= spore.Count)
                        TutorialCourse(spore[i + 1], sporeComplite, sporeText);
                    else
                        break;

#if UNITY_EDITOR
                    if (Input.GetMouseButtonDown(0))
                    {
                        NextCourse();
                        sporeComplite = !sporeComplite;
                        enemyPlant.SetActive(true);
                        PlayerPrefs.SetInt("spore", 1);
                    }
#endif
                    if (Input.touchCount > 0)
                    {
                        if (Input.GetTouch(0).phase == TouchPhase.Began)
                        {
                            NextCourse();
                            sporeComplite = !sporeComplite;
                            enemyPlant.SetActive(true);
                            PlayerPrefs.SetInt("spore", 1);
                        }
                    }
                }
            }

        }

    }

    private void TutorialCourse(GameObject obj, bool complete, string text)
    {
        if (obj != null && !complete)
        {
            cursor.SetActive(true);
            cursor.transform.position = obj.transform.position;
            cursorText.text = text;
            cursorText.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }

    }

    private void TutorialCourse(RectTransform obj, bool complete, string text)
    {
        if (obj != null && !complete && timerTutorial)
        {
            cursor.SetActive(true);
            cursor.transform.position = obj.position;
            cursorText.text = text;
            cursorText.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }

    }

    private void NextCourse()
    {
            //leftComplite = true;
            cursor.SetActive(false);
            cursorText.gameObject.SetActive(false);
            Time.timeScale = 1f;
            timerTutorial = false;
            StartCoroutine(TutorialCooldown());
    }

    IEnumerator TutorialCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        timerTutorial = true;
    }
}