﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetecter : MonoBehaviour
{
    public bool isGrounded;
    public Player player;

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            isGrounded = true;
           /* if (animator != null && player != null) 
            { 
            animator.SetBool("isGrounded", true);
            player.isJumping = false;
            }*/
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            isGrounded = false;
            //StartCoroutine(GroundedCooldown());
            if(player != null)
            Debug.Log(player.IsJumping);

        }
    }
}

