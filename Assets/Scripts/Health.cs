﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] public new Rigidbody2D rigidbody;
    [SerializeField] private int health;
    [SerializeField] private new Camera camera;
    private int armor = 0;
    public int Armor
    {
        get { return armor; }
        set { armor = value; }
    }
    public int CurrentHealth
    {
        get { return health; }
    }
    [SerializeField] public bool damageCoolDown;
    [SerializeField] public bool staticDamageCoolDown;


    private void Start()
    {
        GameManager.Instance.healthContainer.Add(gameObject, this);
    }

    public void TakeHit(int damage)
    {
        if (!staticDamageCoolDown)
        {
            staticDamageCoolDown = true;
            damage -= armor;

            if (damage < 0)
                damage = 0;

            health -= damage;
            StartCoroutine(StaticDamageCoolDown());
            Debug.Log("Нанёсен урон - " + damage + ", по - " + gameObject.name + ", осталось жизни - " + health);
            if (health <= 0)
            {
                if (gameObject.GetComponent<Player>())
                {
                    PlayerDestroyer();
                }
                else
                {
                    Destroy(gameObject);
                }
            }
            if (animator != null)
            animator.SetTrigger("Damage");
        }
    }
    public void TakeHit(int damage, float direction)
    {
        damage -= armor;

        if (damage < 0)
            damage = 0;

        health -= damage;
        Debug.Log(gameObject.name + " Получен урон -" + damage + "\nКоличество жизней -" + health);
        if (health <= 0)
        {
            if (gameObject.GetComponent<Player>())
            {
                PlayerDestroyer();
            }
            else
            {
                Destroy(gameObject);
            }
        }
        if (rigidbody != null && !damageCoolDown)
        {
            damageCoolDown = true;
            rigidbody.velocity = UnityEngine.Vector2.zero;
            if (direction > 0)
                rigidbody.AddForce(new UnityEngine.Vector2(6f, 6f), ForceMode2D.Impulse);
            if (direction < 0)
                rigidbody.AddForce(new UnityEngine.Vector2(-6f, 6f), ForceMode2D.Impulse);
            StartCoroutine(DamageCoolDown());
            if(animator != null)
                animator.SetBool("DamageCoolDown", damageCoolDown);
        }
    }

    public void PlayerDestroyer()
    {
        camera.transform.parent = null;
        camera.enabled = true;
        Destroy(gameObject);
        PauseMenu.Instance.IsDeath();
    }


    public void SetHealth(int bonusHealth)
    {
        health += bonusHealth;
    }

    IEnumerator DamageCoolDown()
    {
        yield return new WaitForSeconds(0.6f);
        damageCoolDown = !damageCoolDown;
        if (animator != null)
            animator.SetBool("DamageCoolDown", damageCoolDown);
    }
    IEnumerator StaticDamageCoolDown()
    {
        yield return new WaitForSeconds(0.5f);
        staticDamageCoolDown = !staticDamageCoolDown;
        if (animator != null)
            animator.SetBool("DamageCoolDown", damageCoolDown);
    }
}
