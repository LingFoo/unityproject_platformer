﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    public int coinsCount;
    [SerializeField] private Text coinsText;
    private List<Item> items;
    public BuffReciever buffReciever;
    public List<Item> Items
    {
        get { return items; }
    }
    public static PlayerInventory Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GameManager.Instance.inventory = this;
        coinsText.text = coinsCount.ToString();
        items = new List<Item>();
    }



    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.coinContainer.ContainsKey(col.gameObject))
        {
            var coin = GameManager.Instance.coinContainer[col.gameObject];
            //coin.OfflineCollider();
            coin.StartDestroy();
            coinsCount++;
            coinsText.text = "X " + coinsCount;
            Debug.Log("Количество монет " + coinsCount);
        }

        if (GameManager.Instance.aidContainer.ContainsKey(col.gameObject))
        {
            var aid = GameManager.Instance.aidContainer[col.gameObject];
            aid.StartAnimator();
            Health health = GetComponent<Health>();
            health.SetHealth(aid.AidPower);
            
            Debug.Log("Количество жизней " + health.CurrentHealth);
            //Destroy(col.gameObject);
        }

        if (GameManager.Instance.itemsContainer.ContainsKey(col.gameObject))
        {
            var item = GameManager.Instance.itemsContainer[col.gameObject];
            items.Add(item.Item);
            item.Destroy(col.gameObject);
        }
    }
}
