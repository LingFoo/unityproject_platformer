﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDamage : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] DamageType damageType;
    [SerializeField] bool destroyWhenBump;
    [SerializeField] bool ignoreEnemy;
    private IObjectDestroyer destroyer;
    private GameObject parent;
    public GameObject Parent
    {
        get { return parent; }
        set { parent = value; }
    }
    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public void Init(IObjectDestroyer destroyer)
    {
        this.destroyer = destroyer;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(ignoreEnemy && col.transform.CompareTag("Enemy"))
        {
            return;
        }

        if (col.gameObject == parent) 
            return;
        if (col.transform.CompareTag("Ladder") || col.transform.CompareTag("Coin"))
        {
            return;
        }

        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
        {
            var health = GameManager.Instance.healthContainer[col.gameObject];
            health.TakeHit(damage);
        }

        if (destroyWhenBump)
        {
            if (destroyer == null)
                Destroy(gameObject);
            else
                destroyer.Destroy(gameObject);
        }
    }


           
    
}

public interface IObjectDestroyer
{
    void Destroy(GameObject gameObject);
}
public enum DamageType
{
    Static, Dinamic
}