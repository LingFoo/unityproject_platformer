using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 10;
    [SerializeField] private float force;
    [SerializeField] private new Rigidbody2D rigidbody;
    [SerializeField] private float minimalHeight;
    [SerializeField] private bool isCheatMode;
    [SerializeField] private GroundDetecter groundDetecter;
    public GroundDetecter GroundDetecter
    {
        get { return groundDetecter; }
    }
    private Vector3 direction;
    public Vector3 Direction { get { return direction; } }

    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] public bool IsJumping { get; set; }
    [SerializeField] private bool onLadder;
    [SerializeField] private Health health;
    [SerializeField] private BuffReciever buffReciever;
    public Vector3 startPosition;
    private UICharacterController controller;
    public bool OnLadder {
        get { return onLadder; }
        set { onLadder = value; }
    }
    public static Player Instance { get; set; }
    public BuffReciever BuffReciever
    {
        get { return buffReciever; }
    }
    public Health Health
    {
        get { return health; }
    }

    [SerializeField] private Image buffTimer;
    [SerializeField] private Image timerImage;
    [SerializeField] private Sprite sword;
    [SerializeField] private Sprite shield;
    [SerializeField] private Sprite lightning;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        startPosition = gameObject.transform.position;
        Time.timeScale = 1;
        buffReciever.OnBuffsChanged += ApplyBuff;
    }

    public void InitUIController(UICharacterController uiController)
    {
        controller = uiController;
        //if(controller.Jump != null)
        //controller.Jump.onClick.AddListener(Jump);
        controller.Fire.onClick.AddListener(PlayerShoot);
        controller.PressedJump.ButtonUp += StopOnLadder;
        if (controller.Jump.GetComponent<EventTrigger>())
        {
            var trigger = controller.Jump.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((data) => { Jump(); });
            trigger.triggers.Add(entry);
        }
    }


    private void ApplyBuff()
    {
        if (buffReciever.buffTemp != null)
        {
            StartCoroutine(BuffTimerStart());
            if (buffReciever.buffTemp.type == BuffType.Force)
                StartCoroutine(ForceBuff());
            if (buffReciever.buffTemp.type == BuffType.Armor)
                StartCoroutine(ArmorBuff());
            if (buffReciever.buffTemp.type == BuffType.Damage)
                Shooting.Instance.BuffArrows(buffReciever.buffTemp.additiveBonus);



        }

    }
    void FixedUpdate()
    {
        Move();

        CheckFall();

        UpOnLadder();
    }

    private void Update()
    {
        if (!onLadder)
        {
            IsJumping = IsJumping && !groundDetecter.isGrounded;
            animator.SetBool("isGrounded", groundDetecter.isGrounded);
            if (!IsJumping && !groundDetecter.isGrounded && !health.damageCoolDown && !onLadder)
            {
                animator.SetTrigger("StartFall");
            }
        }

    }
    public void Move()
    {
        if (!health.damageCoolDown)
        {
            direction = Vector3.zero;
#if UNITY_EDITOR
            if(Input.GetKey(KeyCode.A) && !health.damageCoolDown)
                direction = Vector3.left;

            if (Input.GetKey(KeyCode.D) && !health.damageCoolDown)
                direction = Vector3.right;
#endif
            if (health != null)
            {
                if (controller.Left.IsPressed && !health.damageCoolDown)
                    direction = Vector3.left;

                if (controller.Right.IsPressed && !health.damageCoolDown)
                {
                    direction = Vector3.right;
                }
                direction *= speed;
                direction.y = rigidbody.velocity.y;
                rigidbody.velocity = direction;
            }
        }




        if (direction.x > 0)
            spriteRenderer.flipX = false;
        if (direction.x < 0)
            spriteRenderer.flipX = true;

        animator.SetFloat("Speed", Mathf.Abs(direction.x));
    }
    public void PlayerShoot()
    {
        Shooting.Instance.CheckShoot(rigidbody.velocity);
    }

    public void Jump()
    {
        if (groundDetecter.isGrounded && !onLadder)
        {
            animator.SetTrigger("StartJump");
            IsJumping = true;
            rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            groundDetecter.isGrounded = false;
        }
    }

    private void UpOnLadder()
    {
        if (onLadder && controller.PressedJump.IsPressed)
        {

            rigidbody.velocity = new Vector2(0, 1 * speed); 
            animator.speed = 1;
            animator.SetFloat("UpSpeed", 1);


        }
        else if (onLadder && !controller.Left.IsPressed && !controller.Right.IsPressed)
        {
            float xSpeed = rigidbody.velocity.x;
            rigidbody.velocity = new Vector2(xSpeed, 0);

        }
        if(onLadder && !groundDetecter.isGrounded && !controller.PressedJump.IsPressed)
        {
            animator.SetBool("OnLadder", true);
            animator.SetFloat("UpSpeed", 1);
            animator.Play("OnLadder", 0);
            animator.speed = 0;
            float xSpeed = rigidbody.velocity.x;
            rigidbody.velocity = new Vector2(xSpeed, 0);
        }
        if(!onLadder && animator.speed < 1)
        {
            animator.speed = 1;
            animator.SetBool("OnLadder", false);
        }
        if (!onLadder)
        {
            animator.SetBool("OnLadder", false);
            animator.SetFloat("UpSpeed", 0);
        }
    }
    private void StopOnLadder()
    {
        if (onLadder)
        {
            Debug.Log("����� ��������");
            animator.speed = 0;
            
        }
        else
        {
            animator.SetFloat("UpSpeed", 0);
        }
    }


    void CheckFall()
    {
        if (transform.position.y < minimalHeight && isCheatMode)
        {
            transform.position = startPosition;
            rigidbody.velocity = new Vector2(0, 0);
        }
        else if (transform.position.y < minimalHeight && !isCheatMode)
            health.PlayerDestroyer();
    }
    
    IEnumerator ForceBuff()
    {
        float forceTemp = force;
        force += buffReciever.buffTemp.additiveBonus;
        yield return new WaitForSeconds(5);

        force = forceTemp;
        yield break;
    }

    IEnumerator ArmorBuff()
    {
        health.Armor = buffReciever.buffTemp.additiveBonus;
        yield return new WaitForSeconds(5);

        health.Armor = 0;
        yield break;
    }

    IEnumerator BuffTimerStart()
    {
        float buffTime = 0;
        timerImage.gameObject.SetActive(true);
        if (buffReciever.buffTemp.type == BuffType.Force)
        {
            timerImage.sprite = lightning;
            buffTime = 0.05f;
        }

        if (buffReciever.buffTemp.type == BuffType.Armor)
        {
            timerImage.sprite = shield;
            buffTime = 0.05f;
        }

        if (buffReciever.buffTemp.type == BuffType.Damage)
        {
            timerImage.sprite = sword;
            buffTime = 0.1f;
        }

        for (float i = 0; i < 100; i++)
        {
            buffTimer.fillAmount = i / 100;
            yield return new WaitForSeconds(buffTime);
        }
        timerImage.gameObject.SetActive(false);
        yield break;


    }



}
