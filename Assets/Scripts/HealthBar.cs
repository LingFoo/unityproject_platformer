﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image health;
    [SerializeField] private float delta;
    [SerializeField] private float currentHealth;
    [SerializeField] private float healthValue;
    private Player player;

    void Start()
    {
        player = FindObjectOfType<Player>();
        healthValue = player.Health.CurrentHealth / 100.0f;
    }


    void Update()
    {
        currentHealth = player.Health.CurrentHealth / 100.0f;

        if (Math.Abs(currentHealth - healthValue) < delta)
        {
            healthValue = currentHealth;
        }

        if (currentHealth > healthValue)
        {
            healthValue += delta;
        }

        if (currentHealth < healthValue)
        {
            healthValue -= delta;
        }
        
        
        health.fillAmount = healthValue;
    }
}
