﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDamage : MonoBehaviour
{
    public int damage = 10;
    public string collisionTag;
    public bool isAttack;
    public Animator animator;
    public Health health;
    private float direction;
    public float Direction
    {
        get { return direction; }
    }


    private void OnCollisionStay2D(Collision2D col)
    {
        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
        {
            health = GameManager.Instance.healthContainer[col.gameObject];
            direction = (col.transform.position - transform.position).x;

            if (!isAttack)
            {
                isAttack = true;
                if (animator != null)
                {
                    animator.SetFloat("Direction", Mathf.Abs(direction));
                }
                else
                {
                    SetDamage();
                }
                StartCoroutine(AttackCoolDown());
            }
        }
        else
        {
            return;
        }
    }


    public void SetDamage()
    {
        if(health != null)
            health.TakeHit(damage, direction);
        health = null;
        direction = 0f;
        if(animator != null)
            animator.SetFloat("Direction", 0f);
    }

    public void StaticDamage()
    {
        if (health != null)
            health.TakeHit(damage);
        health = null;
        direction = 0f;
        if (animator != null)
            animator.SetFloat("Direction", 0f);
    }

    IEnumerator AttackCoolDown()
    {
        yield return new WaitForSeconds(1f);
        isAttack = !isAttack;
    }

}