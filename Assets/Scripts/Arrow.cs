﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour, IObjectDestroyer
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private new Rigidbody2D rigidbody;
    [SerializeField] private float force;
    [SerializeField] private float lifeTime;
    [SerializeField] private TriggerDamage triggerDamage;
    public TriggerDamage TriggerDamage
    {
        get { return triggerDamage; }
        set { triggerDamage = value; }
    }
    private Shooting player;
    //[SerializeField] public GameObject player;
    public float Force
    {
        get { return force; }
        set { force = value; }
    }
    public void Destroy(GameObject gameObject)
    {
        player.ReturnArrowtoPool(this);
    }

    public void SetImpulse(Vector2 direction, float force, Shooting player)
    {
        if (force < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        this.player = player;
        triggerDamage.Parent = player.gameObject;
        triggerDamage.Init(this);
        rigidbody.AddForce(direction * force, ForceMode2D.Impulse);
        StartCoroutine(StartLife());
    }

    private IEnumerator StartLife()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
        yield break;
    }
}
