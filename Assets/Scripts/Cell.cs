﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{

    [SerializeField] private Image icon;
    [SerializeField] private Text text;
    private Item item;


    public void Init(Item item)
    {
        this.item = item;
        icon.sprite = item == null? null:item.Icon;
        text.text = item == null ? null : item.ItemName;
    }

    public void OnClickCell()
    {
        if (item == null)
            return;

        GameManager.Instance.inventory.Items.Remove(item);
        Buff buff = new Buff
        {
            type = item.Type,
            additiveBonus = item.Value
        };
        GameManager.Instance.inventory.buffReciever.AddBuff(buff);
        GameManager.Instance.inventoryUI.InitCells();
    }

}
